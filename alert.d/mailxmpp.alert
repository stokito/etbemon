#!/usr/bin/perl
use strict;
use BSD::Resource;
#
# mailxmpp.alert - Mail and xmpp alert for mon
#
# The first line from STDIN is summary information, adequate to send
# to a pager or email subject line.
#
# -f from@addr.x   set the smtp envelope "from" address
# -x destination xmpp address
# -m destination smtp address
#
# Jim Trocki, trockij@arctic.org
# XMPP added by Russell Coker russell@coker.com.au
#
# $Id: mail.alert,v 1.3 2005/04/17 07:42:26 trockij Exp $
#
#    Copyright (C) 1998, Jim Trocki
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
setrlimit(RLIMIT_CPU, 5, 5) or die "Can't set CPU time limit to 5 seconds";

use Getopt::Std;
use Text::Wrap;

our $opt_S;
our $opt_s;
our $opt_g;
our $opt_h;
our $opt_l;
our $opt_f;
our $opt_t;
our $opt_u;
our $opt_m;
our $opt_x;
getopts ("S:s:g:h:t:l:f:um:x:");

my $summary=<STDIN>;
chomp $summary;

$summary = $opt_S if (defined $opt_S);

use Sys::Hostname;
my $host = hostname;

my $mailfrom = "-f $opt_f -F $opt_f" if (defined $opt_f);

my $ALERT = $opt_u ? "UPALERT" : "ALERT";

my $t = localtime($opt_t);
my ($wday,$mon,$day,$tm) = split (/\s+/, $t);

open (MAIL, "| /usr/lib/sendmail -oi -t $mailfrom") ||
    die "could not open pipe to mail: $!\n";
my @xmpprec = split(/,/, $opt_x);
open (XMPP, "| /usr/bin/go-sendxmpp @xmpprec") ||
    die "could not open pipe to sendxmpp: $!\n";
print MAIL <<EOF;
To: $opt_m
Subject: $ALERT $opt_g/$opt_s: $summary ($wday $mon $day $tm)
X-Mailer: $0

EOF
print XMPP <<EOF;
$ALERT $opt_g/$opt_s: $summary ($wday $mon $day $tm)
EOF

print MAIL <<EOF;

Group                 : $opt_g
Service               : $opt_s
Time noticed          : $t
Secs until next alert : $opt_l
EOF

print XMPP <<EOF;

Group                 : $opt_g
Service               : $opt_s
Time noticed          : $t
Secs until next alert : $opt_l
EOF

print MAIL wrap ("", "\t\t\t", "Members               : $opt_h"), "\n";
print XMPP wrap ("", "\t\t\t", "Members               : $opt_h"), "\n";

print MAIL <<EOF;

Detailed text (if any) follows:
-------------------------------
EOF

print XMPP <<EOF;

Detailed text (if any) follows:
-------------------------------
EOF

#
# The remaining lines normally contain more detailed information,
# but this is monitor-dependent.
#
while (<STDIN>) {
    print MAIL;
    print XMPP;
}
close (MAIL);
close (XMPP);
