#!/usr/bin/perl
use strict;
use Getopt::Std;

# Mon script to check HP server temperature via a setuid wrapper around the
# "hplog" program.
#
# Takes any line containing "Normal" to be good, anything else is bad.
# Copyright Russell Coker GPLv3
#
# Use -i to specify a comma separated list of ID numbers to ignore, EG
# -i2,4
# Use -f for Fahrenheit

open(SENSORS, "/usr/lib/mon/bin/hplog.helper|") or die "Can't run hplog";

my $line_id = "";
my $input = 0;
my $summary = "";
my $failed = "";
my @ignore_list;
my $temp_ignore = "[- 1-9][- 0-9][-0-9]F.";

our $opt_f;
our $opt_i;
getopts("i:f");

if($opt_f)
{
  $temp_ignore = ".[- 1-9][- 0-9][-0-9]C";
}

if(length($opt_i) > 0)
{
  foreach my $item (split(/,/, $opt_i))
  {
    $ignore_list[$item] = 1;
  }
}

# this is written for a hplog output line having 2 characters for the ID num, a
# space, then 28 characters for the description, 8 characters for the status,
# and then temperatures.

my $goodcount = 0;
while(<SENSORS>)
{
  chomp;
  next if(not $_ =~ /^[ 1-9][0-9] /);
  if($_ =~ /Normal/)
  {
    $goodcount++;
    next;
  }

  $line_id = substr($_, 0, 2);
  next if($ignore_list[$line_id] == 1);

  my $name = substr($_, 4, 28);
  $name =~ s/ *$//;
  $name =~ s/Basic Sensor/Bas:/;
  $summary .= ", " if(length($summary) > 0);
  $summary .= $name;
  $_ =~ s/$temp_ignore//g;
  $failed .= "$_\n";
}
if(length($failed) == 0)
{
  print "$goodcount sensors ok\n";
  exit(0);
}
print "$summary\n";
print $failed;

my $format = "%8s %5s %4s %8s %s\n";
my $ccount = 0;
my $output = "";

use Proc::ProcessTable;
my $process_table = new Proc::ProcessTable('cache_ttys' => 0 );

foreach my $p ( sort { $b->pctcpu <=> $a->pctcpu } @{$process_table->table} )
{
  if($ccount > 9 || $p->pctcpu < 5.0)
  {
    last;
  }
  $ccount++;
  my $name;
  $name  = getpwuid($p->uid) or $name = $p->uid;
  my $cmd = $p->cmndline, 0, 60;
  if(length($cmd) == 0)
  {
    $cmd = "[" . $p->fname . "]";
  }
  $cmd = substr($cmd, 0, 60);
  my $devname = $p->ttydev;
  $devname =~ s/\/dev\///;
  $output .= sprintf($format, $name, sprintf("%5d", $p->pid), sprintf("%4.1f", $p->pctcpu), $devname, $cmd);
}
if($ccount > 0)
{
  print "\nHere are processes with the top CPU percentages:\n";
  printf($format, "USER", "PID", "CPU", "TTY", "COMMAND");
  print "$output\n";
}

exit(1);
