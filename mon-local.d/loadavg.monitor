#!/usr/bin/perl
use strict;

# Mon script to check load average
# 3 load average numbers on the command line
# optional 4th parameter is the maximum percentage of swap+RAM used (default 90)
# RAM used is deemed to be MemTotal-MemAvailable

# Copyright Russell Coker GPLv3

if ( $#ARGV < 2 || $#ARGV > 3 )
{
  print STDERR "Usage min 5min 15min [%swap]\n";
  exit 1;
}

my $SwapAlert = 90;
if($#ARGV == 3)
{
  $SwapAlert = $ARGV[3];
}

open(LOAD, "</proc/loadavg") or die "Can't open /proc/loadavg";
my $load = <LOAD>;
close(LOAD);
chomp $load;
my @arr = split(/ /, $load);

my $SwapTotal;
my $SwapFree;
my $MemTotal;
my $MemAvailable;

open(MEMINFO, "</proc/meminfo") or die "Can't open /proc/meminfo";
while(<MEMINFO>)
{
  chomp;
  if($_ =~ /SwapTotal/)
  {
    $_ =~ s/ kB.*$//;
    $_ =~ s/^.* //;
    $SwapTotal = $_;
  }
  elsif($_ =~ /SwapFree/)
  {
    $_ =~ s/ kB.*$//;
    $_ =~ s/^.* //;
    $SwapFree = $_;
  }
  elsif($_ =~ /MemTotal/)
  {
    $_ =~ s/ kB.*$//;
    $_ =~ s/^.* //;
    $MemTotal = $_;
  }
  elsif($_ =~ /MemAvailable/)
  {
    $_ =~ s/ kB.*$//;
    $_ =~ s/^.* //;
    $MemAvailable = $_;
  }
}
close(MEMINFO);
my $SwapUsed = $SwapTotal - $SwapFree;

my $summary;
if($arr[0] > $ARGV[0] || $arr[1] > $ARGV[1] || $arr[2] > $ARGV[2])
{
  $summary = "$arr[0] $arr[1] $arr[2] >= $ARGV[0] $ARGV[1] $ARGV[2]";
}

my $SwapPercent = ($SwapUsed + $MemTotal - $MemAvailable) * 100 / ($SwapTotal + $MemTotal);
if($SwapPercent > $SwapAlert)
{
  if($summary)
  {
    $summary .= "   ";
  }
  $summary .= sprintf("Swap+RAM Use: %d%% > %d%%", $SwapPercent, $SwapAlert);
}

if(length($summary) == 0)
{
  exit(0);
}

print("$summary\n");

# https://stackoverflow.com/questions/37124052/explain-perl-code-to-display-a-number-of-bytes-in-kb-mb-gb-etc
# thanks Borodin
sub fmt2 {
    my ($n) = @_;
    my @suffix = ( '', qw/ K M G T P E / );

    my $i = 0;
    until ( $n < 1000 or $i == $#suffix ) {
        $n /= 1024;
        ++$i;
    }

    sprintf $i ? '%.3g%sB' : '%.0f%sB', $n, $suffix[$i];
}

use Proc::ProcessTable;
my $process_table = new Proc::ProcessTable('cache_ttys' => 0 );

my $format = "%8s %5s %6s %6s %8s %s\n";

my $output = "";
my $dcount = 0;

foreach my $process ( @{$process_table->table} )
{
  if($process->state eq "uwait")
  {
    $dcount++;
    my $name;
    $name  = getpwuid($process->uid) or $name = $process->uid;
    my $cmd = $process->cmndline, 0, 60;
    if(length($cmd) == 0)
    {
      $cmd = "[" . $process->fname . "]";
    }
    $cmd = substr($cmd, 0, 60);
    my $devname = $process->ttydev;
    $devname =~ s/\/dev\///;
    $output .= sprintf($format, $name, sprintf("%5d", $process->pid), fmt2($process->size), fmt2($process->rss), $devname, $cmd);
  }
}

if($dcount > 0)
{
    print "Here are D state processes:\n";
    printf($format, "USER", "PID", "VSZ", "RSS", "TTY", "COMMAND");
    print $output;
}

$format = "%8s %5s %4s %8s %s\n";
my $ccount = 0;
$output = "";
foreach my $p ( sort { $b->pctcpu <=> $a->pctcpu } @{$process_table->table} )
{
  if($ccount > 9 || $p->pctcpu < 5.0)
  {
    last;
  }
  $ccount++;
  my $name;
  $name  = getpwuid($p->uid) or $name = $p->uid;
  my $cmd = $p->cmndline, 0, 60;
  if(length($cmd) == 0)
  {
    $cmd = "[" . $p->fname . "]";
  }
  $cmd = substr($cmd, 0, 60);
  my $devname = $p->ttydev;
  $devname =~ s/\/dev\///;
  $output .= sprintf($format, $name, sprintf("%5d", $p->pid), sprintf("%4.1f", $p->pctcpu), $devname, $cmd);
}
if($ccount > 0)
{
  if($dcount > 0)
  {
    print "\n";
  }
  print "Here are processes with the top CPU percentages:\n";
  printf($format, "USER", "PID", "CPU", "TTY", "COMMAND");
  print "$output";
}

$format = "%8s %5s %6s %6s %8s %s\n";
my $rcount = 0;
$output = "";
foreach my $p ( sort { $b->rss <=> $a->rss } @{$process_table->table} )
{
  if(($p->rss * 100 / 1024 / $MemTotal) < 2.0 or $rcount > 9)
  {
    last;
  }
  $rcount++;
  my $name;
  $name  = getpwuid($p->uid) or $name = $p->uid;
  my $cmd = $p->cmndline, 0, 60;
  if(length($cmd) == 0)
  {
    $cmd = "[" . $p->fname . "]";
  }
  $cmd = substr($cmd, 0, 60);
  my $devname = $p->ttydev;
  $devname =~ s/\/dev\///;
  $output .= sprintf($format, $name, sprintf("%5d", $p->pid), fmt2($p->size), fmt2($p->rss), $devname, $cmd);
}
if($rcount > 0)
{
  if($dcount > 0 or $ccount > 0)
  {
    print "\n";
  }
  print "Here are processes with the top RAM use:\n";
  printf($format, "USER", "PID", "VIRT", "RES", "TTY", "COMMAND");
  print "$output";
}

printf("\nSwap Used: %s / %s\n", fmt2($SwapUsed*1024), fmt2($SwapTotal*1024));

use File::Find;
use Cwd;

my $cgmount = "/sys/fs/cgroup";
my $cgmountlength = length($cgmount);

my %cpuexclude;
my %ioexclude;
my @iopressure;
my @cpupressure;

sub wanted
{
  if($_ eq "cpu.pressure" or $_ eq "io.pressure")
  {
    my $dir = getcwd();
    my @dir_parts = split(/\//, $dir);
    pop(@dir_parts);
    my $parent = join('/', @dir_parts);

    my $type;
    if($_ eq "cpu.pressure")
    {
      if($cpuexclude{$parent} == 1)
      {
        $ioexclude{$dir} = 1;
        return;
      }
      $type = "cpu";
    }
    else
    {
      if($ioexclude{$parent} == 1)
      {
        $ioexclude{$dir} = 1;
        return;
      }
      $type = "io";
    }
    open(P, $_) or return;
    my $line = <P>;
    my $some = (split(/ /, (split(/=/, $line))[1]))[0];
    if($some < 0.5)
    {
      if($type eq "cpu")
      {
        $cpuexclude{$dir} = 1;
      }
      else
      {
        $ioexclude{$dir} = 1;
      }
    }
    else
    {
      $line = substr($line, 5);
      $dir = substr($dir, $cgmountlength);
      if($type eq "cpu")
      {
        push(@cpupressure, "$dir $line");
      }
      else
      {
        push(@iopressure, "$dir $line");
        $line = <P>;
        push(@iopressure, "$dir $line");
      }
    }
    close(P);
  }
}
find(\&wanted, ( $cgmount ));

if($#cpupressure != -1)
{
  print "\nSystem CPU Pressure:";
  foreach (@cpupressure)
  {
    print "$_";
  }
}

if($#iopressure != -1)
{
  print "\nSystem IO Pressure:";
  foreach (@iopressure)
  {
    print "$_";
  }
  print "\n";
}

exit(1);
